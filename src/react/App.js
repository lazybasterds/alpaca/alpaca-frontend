import React, { Component } from "react";
import "./App.scss";
import DateWeather from "./components/date-weather/DateWeather";
import SearchBar from "./components/search-bar/SearchBar";
import { EventEmitter } from "events";
import SearchRenderer from "./components/search-renderer/SearchRenderer";
import DirectoryLayout from "./components/directory/DirectoryLayout";

import TileButton from "./components/tile-button/TileButton";
import QuickSearch from "./components/quick-search/QuickSearch";
import MapScene from "./components/map/MapScene";

class App extends Component {
  constructor() {
    super();

    this.state = {
      searchString: "",
      isDirectoryActive: false,
      isMapActive: false
    };

    this.searchBarEmitter = new EventEmitter();
    this.quickSearchEmitter = new EventEmitter();
    this.freqSearchEmitter = new EventEmitter();
    this.directoryButtonEmitter = new EventEmitter();
    this.mapButtonEmitter = new EventEmitter();

    this.searchBarEmitter.addListener("onChange", data => {
      this.setState({
        searchString: data.searchString
      });
    });

    this.directoryButtonEmitter.addListener("onClick", data => {
      this.setState({
        isDirectoryActive: data
      });
    });

    this.mapButtonEmitter.addListener("onClick", data => {
      this.setState({
        isMapActive: data
      });
    });

    this.quickSearchEmitter.addListener("onClick", data => {
      this.setState({
        searchString: data.searchString
      });
    });
  }

  render() {
    return (
      <div className="tultul">
        <div
          className={`grid-area-left ${
            this.state.isDirectoryActive ? "hidden" : null
          }`}
        >
          <DateWeather />
          <SearchBar
            emitter={this.searchBarEmitter}
            searchString={this.searchString}
          />
          <div className="quick-search__wrapper">
            <QuickSearch
              title="Quick Search"
              emitter={this.quickSearchEmitter}
            />
            <QuickSearch
              title="Frequently Searched"
              emitter={this.freqSearchEmitter}
            />
          </div>
        </div>
        <SearchRenderer
          classes={["grid-area-search"]}
          searchString={this.state.searchString}
          visible={this.state.searchString !== ""}
        />
        <div
          className={`grid-area-ad ${
            this.state.isDirectoryActive || this.state.searchString !== ""
              ? "hidden"
              : null
          }`}
        >
          ad ad ad ad ad ad ad ad ad ad ad ad ad ad ad
        </div>
        <DirectoryLayout
          classes={["grid-area-dir"]}
          visible={this.state.isDirectoryActive}
        />
        {/* <MapScene 
                    classes={['grid-area-map']} 
                    visible={this.state.isMapActive}
                /> */}
        <TileButton
          classes={["grid-area-dir-btn"]}
          label="View Directory"
          emitter={this.directoryButtonEmitter}
          icon="ic_map_white_48dp.png"
          visible={
            this.state.searchString === "" && !this.state.isDirectoryActive
          }
        />
        <TileButton
          classes={["grid-area-map-btn"]}
          label="Explore Map"
          emitter={this.mapButtonEmitter}
          icon="ic_layers_white_48dp.png"
          visible={
            this.state.searchString === "" && !this.state.isDirectoryActive
          }
        />
      </div>
    );
  }
}

export default App;
