import React, { Component } from 'react';
import * as THREE from 'three';
import './MapScene.scss';

class MapScene extends Component{
    componentDidMount(){
        const width = this.mount.clientWidth;
        const height = this.mount.clientHeight;

        this.clock = new THREE.Clock();

        // scene
        this.scene = new THREE.Scene();

        // camera
        this.camera = new THREE.PerspectiveCamera( 75, width / height, 0.1, 1000 );
        this.camera.position.y = 5;
        this.camera.position.z = 5;
        this.camera.lookAt(new THREE.Vector3(0, 0, 0));

        // renderer
        this.renderer = new THREE.WebGLRenderer({ antialias: true })
        this.renderer.setClearColor('#000000');
        this.renderer.setSize(width, height);
        this.mount.appendChild(this.renderer.domElement);

        // test cube
        const geometry = new THREE.BoxGeometry(1, 1, 1);
        const material = new THREE.MeshPhongMaterial({ color: '#433F81', dithering: true, transparent: true, opacity: 0.5 });
        this.cube = new THREE.Mesh(geometry, material);
        this.scene.add(this.cube);

        // light
        this.light1 = new THREE.PointLight( 0xefefef, 2, 50 );
        // this.light1.position.x = 5;
        this.light1.position.y = 5;
        this.light1.position.z = 5;
        const sphere = new THREE.SphereBufferGeometry( 0.5, 16, 8 );
        this.light1.add( new THREE.Mesh( sphere, new THREE.MeshBasicMaterial( { color: 0xff0040 } ) ) );
        this.scene.add( this.light1 );

        this.start()
    }
    componentWillUnmount() {
        this.stop();
        this.mount.removeChild(this.renderer.domElement);
    }
    start = () => {
        if (!this.frameId) {
            this.frameId = requestAnimationFrame(this.animate);
        }
    }
    stop = () => {
        cancelAnimationFrame(this.frameId);
    }
    animate = () => {
        // this.cube.rotation.x += 0.01;
        // var time = Date.now() * 0.0005;
        // var delta = this.clock.getDelta();
        // if ( object ) object.rotation.y -= 0.5 * delta;
        // this.light1.position.x = Math.sin( time * 0.7 ) * 5;
        // this.light1.position.y = Math.cos( time * 0.5 ) * 10;
        // this.light1.position.z = Math.cos( time * 0.3 ) * 5;
        
        this.cube.rotation.y += 0.01;
        this.renderScene();
        this.frameId = window.requestAnimationFrame(this.animate);
    }
    renderScene = () => {
        this.renderer.render(this.scene, this.camera);
    }
    render(){
        /* if (!this.props.visible) return null;
        else  */return(
            <div
                // style={{ width: '100%', height: '100%', display: this.props.visible ? 'block' : 'none' }}
                style={{ width: '100%', height: '100%' }}
                className={`map ${this.props.classes.join(' ')}`}
                ref={(mount) => { this.mount = mount }}
            />
        )
    }
}

export default MapScene;