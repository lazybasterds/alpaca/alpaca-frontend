import React, { Component } from 'react';
import './TileButton.scss';

class TileButton extends Component {
    constructor(props) {
        super(props);
        this.tapped = this.tapped.bind(this);
    }
    tapped() {
        setTimeout( ()=> {
            this.props.emitter.emit('onClick', {
                isActive: true
            });
        }, 0);
    }
    render() {
        if(!this.props.visible) return null;
        else return (
            <div className={`tile-button ${this.props.classes.join(' ')}`} onClick={this.tapped}>
                <div>
                    <img 
                        src={require(`../../assets/images/icons/${this.props.icon}`)}
                    />
                    <h2>
                        {this.props.label}
                    </h2>
                </div>
            </div>
        )
    }
}

export default TileButton;