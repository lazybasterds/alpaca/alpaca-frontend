import React, { Component } from 'react';
import './Weather.scss';
class Weather extends Component {

    render() {
        return (
            <div className='weather'>
                <div className='temperature'>
                    {/* TODO on tap change unit */}
                    {`${Math.floor(this.props.weather.main.tempinc)}°c`}
                </div>
            </div>
        )
    }
}
export default Weather;
