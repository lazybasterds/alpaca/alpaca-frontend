/**
 * @flow
 * @relayHash d121d629d319e93b7a6d430eb13e2eb9
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type DateWeatherQueryVariables = {||};
export type DateWeatherQueryResponse = {|
  +getCurrentTime: ?{|
    +unixtime: ?string
  |},
  +getWeather: ?{|
    +name: ?string,
    +base: ?string,
    +main: ?{|
      +tempinc: ?number
    |},
  |},
|};
export type DateWeatherQuery = {|
  variables: DateWeatherQueryVariables,
  response: DateWeatherQueryResponse,
|};
*/


/*
query DateWeatherQuery {
  getCurrentTime(timezone: "Asia/Singapore") {
    unixtime
    id
  }
  getWeather(locale: "singapore") {
    name
    base
    main {
      tempinc
    }
    id
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "Literal",
    "name": "timezone",
    "value": "Asia/Singapore",
    "type": "String!"
  }
],
v1 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "unixtime",
  "args": null,
  "storageKey": null
},
v2 = [
  {
    "kind": "Literal",
    "name": "locale",
    "value": "singapore",
    "type": "String!"
  }
],
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "name",
  "args": null,
  "storageKey": null
},
v4 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "base",
  "args": null,
  "storageKey": null
},
v5 = {
  "kind": "LinkedField",
  "alias": null,
  "name": "main",
  "storageKey": null,
  "args": null,
  "concreteType": "mainType",
  "plural": false,
  "selections": [
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "tempinc",
      "args": null,
      "storageKey": null
    }
  ]
},
v6 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
};
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "DateWeatherQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": [],
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "getCurrentTime",
        "storageKey": "getCurrentTime(timezone:\"Asia/Singapore\")",
        "args": (v0/*: any*/),
        "concreteType": "timeType",
        "plural": false,
        "selections": [
          (v1/*: any*/)
        ]
      },
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "getWeather",
        "storageKey": "getWeather(locale:\"singapore\")",
        "args": (v2/*: any*/),
        "concreteType": "weatherType",
        "plural": false,
        "selections": [
          (v3/*: any*/),
          (v4/*: any*/),
          (v5/*: any*/)
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "DateWeatherQuery",
    "argumentDefinitions": [],
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "getCurrentTime",
        "storageKey": "getCurrentTime(timezone:\"Asia/Singapore\")",
        "args": (v0/*: any*/),
        "concreteType": "timeType",
        "plural": false,
        "selections": [
          (v1/*: any*/),
          (v6/*: any*/)
        ]
      },
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "getWeather",
        "storageKey": "getWeather(locale:\"singapore\")",
        "args": (v2/*: any*/),
        "concreteType": "weatherType",
        "plural": false,
        "selections": [
          (v3/*: any*/),
          (v4/*: any*/),
          (v5/*: any*/),
          (v6/*: any*/)
        ]
      }
    ]
  },
  "params": {
    "operationKind": "query",
    "name": "DateWeatherQuery",
    "id": null,
    "text": "query DateWeatherQuery {\n  getCurrentTime(timezone: \"Asia/Singapore\") {\n    unixtime\n    id\n  }\n  getWeather(locale: \"singapore\") {\n    name\n    base\n    main {\n      tempinc\n    }\n    id\n  }\n}\n",
    "metadata": {}
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '06e4ae0b9e7fbd00a54e980f8aa9d5c2';
module.exports = node;
