import React, { Component } from 'react';
import './DateWeather.scss';
import { QueryRenderer } from 'react-relay';
import { graphql } from 'babel-plugin-relay/macro';
import environment from '../../../graphql/environment';
import DateTime from './DateTime';
import Weather from './Weather';

class DateWeather extends Component {
    render() {
        return (
            <QueryRenderer
                environment={environment}
                query={ graphql`
                    query DateWeatherQuery {
                        getCurrentTime(timezone: "Asia/Singapore") {
                            unixtime
                        },
                        getWeather(locale: "singapore") {
                            name,
                            base,
                            main {
                                tempinc
                            }
                        }
                    }
                ` }
                variables={{}}
                render={({ error, props }) => {
                    if (error) return <div>Error!</div>;
                    if (!props) return <div>Loading...</div>;
                    return <div className='date-weather'>
                        {/* TODO on tap change time format */}
                        <DateTime unixtime={props.getCurrentTime.unixtime} />
                        <Weather weather={props.getWeather} />
                    </div>
                }}
            />
        )
    }
}
export default DateWeather;
