import React, { Component } from 'react';
import './DateTime.scss'

class DateTime extends Component {
    constructor(props) {
        super(props);
        
        this.days = [
            'sunday', 'monday', 'tuesday',
            'wednesday', 'thursday', 'friday',
            'saturday'
        ];
        this.months = [
            'january', 'february', 'march',
            'april', 'may', 'june',
            'july', 'august', 'september',
            'october', 'november', 'december'
        ]

        this.state = {
            unixtime: parseInt(this.props.unixtime*1000),
            date: new Date()
        }

        setInterval( ()=> {
            this.setState( (state) => {
                return { 
                    unixtime: state.unixtime += 1000,
                    date: new Date(state.unixtime)
                };
            });
        }, 1000)
    }
    render() {
        return (
            <div className='date-time'>
                <div className='time'>
                    {
                        `0${this.state.date.getHours()}`.slice(-2)
                    }<span>:</span>
                    {
                        `0${this.state.date.getMinutes()}`.slice(-2)
                    }
                </div>
                <div className='date'>
                    {
                        `0${this.state.date.getDate()}`.slice(-2)
                    }
                    &nbsp;
                    {
                        this.months[this.state.date.getMonth()]
                    }
                    <span>,</span>&nbsp;
                    {
                        this.state.date.getFullYear()
                    }
                    <br/>
                    {
                        this.days[this.state.date.getDay()]
                    }
                </div>
            </div> 
        )
    }
}

export default DateTime;