import React, { Component } from 'react';
// import { EventEmitter } from 'events';
import './SearchBar.scss';
import { debounce } from 'throttle-debounce';
// import { QueryRenderer } from 'react-relay';
// import { graphql } from 'babel-plugin-relay/macro';
// import environment from '../../../graphql/environment';

class SearchBar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            searchString: ''
        }

        this.search = this.search.bind(this);
        this.debouncedSearch = debounce(1000, this.emitSearch);
    }
    emitSearch() {
        this.props.emitter.emit('onChange', {
            searchString: this.state.searchString
        });
    }
    search(e) {
        this.setState({ searchString: e.target.value }, () => {
            this.debouncedSearch();
        });
    }
    render() {
        return (
            <div className='search-bar'>
                <input 
                    placeholder='Tap here to search...'
                    value={this.state.searchString || this.props.searchString}
                    onChange={this.search}
                />
            </div>
        )
    }
    componentWillUnmount() {
        this.props.emitter.removeAllListeners();
    }
}

export default SearchBar;