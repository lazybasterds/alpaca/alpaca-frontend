import React, { Component } from 'react';
import { QueryRenderer } from 'react-relay';
import { graphql } from 'babel-plugin-relay/macro';
import environment from '../../../graphql/environment';
import { Draggable } from 'gsap/all';
import ThrowPropsPlugin from '../../plugins/ThrowPropsPlugin.js';
import './SearchRenderer.scss';
import SearchRendererItem from './search-renderer-item/SearchRendererItem';

const plugin = ThrowPropsPlugin;

class SearchRenderer extends Component {
    initDraggable() {
        setTimeout( () => {
            const draggable = document.querySelector('.search-draggable');
            const searchContainer = document.querySelector('.grid-area-search');
            if(draggable.offsetHeight > 150/* searchContainer.offsetHeight */) { 
                Draggable.create( draggable, {
                    type: 'y',
                    bounds: searchContainer,
                    edgeResistance: 0.75,
                    throwProps: true,
                    lockAxis: true,
                    dragClickables: true
                })
            }
        }, 0);
    }
    render() {
        if(!this.props.visible) return null;
        else return (
            <QueryRenderer
                environment={environment}
                query={graphql`
                    query SearchRendererQuery($query: String) {
                        search(query: $query) {
                            name,
                            details {
                                description,
                                category,
                            },
                            floor {
                                name
                            },
                            details {
                                logo {
                                    data
                                },
                                image {
                                    data
                                }
                            }
                        }
                    }
                `}
                variables={{
                    query: this.props.searchString
                }}
                render={
                    ({ error, props }) => {
                        // TODO error
                        // if (error) return <div>Error!</div>;
                        // TODO preloader
                        if (!props) return <div>Loading!</div>;

                        // TODO animation
                        return <div className={`search-renderer ${this.props.classes.join(' ')}`}>
                            <div className='search-draggable'>
                            {
                                this.initDraggable()
                            }
                            {
                                props.search.map( (search, i) => {
                                    return <SearchRendererItem 
                                        searchItem={search}
                                        key={i}
                                    />
                                })
                            }
                            </div>
                        </div>
                    }
                }
            />
        )
    }
}

export default SearchRenderer;