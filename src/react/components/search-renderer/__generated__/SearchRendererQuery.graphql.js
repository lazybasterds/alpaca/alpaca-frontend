/**
 * @flow
 * @relayHash acbe5373af96d9617deb94c0f7423ca7
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type SearchRendererQueryVariables = {|
  query?: ?string
|};
export type SearchRendererQueryResponse = {|
  +search: ?$ReadOnlyArray<?{|
    +name: ?string,
    +details: ?{|
      +description: ?string,
      +category: ?string,
      +logo: ?{|
        +data: ?string
      |},
      +image: ?{|
        +data: ?string
      |},
    |},
    +floor: ?{|
      +name: ?string
    |},
  |}>
|};
export type SearchRendererQuery = {|
  variables: SearchRendererQueryVariables,
  response: SearchRendererQueryResponse,
|};
*/


/*
query SearchRendererQuery(
  $query: String
) {
  search(query: $query) {
    name
    details {
      description
      category
      logo {
        data
        id
      }
      image {
        data
        id
      }
    }
    floor {
      name
      id
    }
    id
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "query",
    "type": "String",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "query",
    "variableName": "query",
    "type": "String"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "name",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "description",
  "args": null,
  "storageKey": null
},
v4 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "category",
  "args": null,
  "storageKey": null
},
v5 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "data",
  "args": null,
  "storageKey": null
},
v6 = [
  (v5/*: any*/)
],
v7 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v8 = [
  (v5/*: any*/),
  (v7/*: any*/)
];
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "SearchRendererQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": (v0/*: any*/),
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "search",
        "storageKey": null,
        "args": (v1/*: any*/),
        "concreteType": "nodeType",
        "plural": true,
        "selections": [
          (v2/*: any*/),
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "details",
            "storageKey": null,
            "args": null,
            "concreteType": "nodeDetailsType",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              (v4/*: any*/),
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "logo",
                "storageKey": null,
                "args": null,
                "concreteType": "imageType",
                "plural": false,
                "selections": (v6/*: any*/)
              },
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "image",
                "storageKey": null,
                "args": null,
                "concreteType": "imageType",
                "plural": false,
                "selections": (v6/*: any*/)
              }
            ]
          },
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "floor",
            "storageKey": null,
            "args": null,
            "concreteType": "floorType",
            "plural": false,
            "selections": [
              (v2/*: any*/)
            ]
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "SearchRendererQuery",
    "argumentDefinitions": (v0/*: any*/),
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "search",
        "storageKey": null,
        "args": (v1/*: any*/),
        "concreteType": "nodeType",
        "plural": true,
        "selections": [
          (v2/*: any*/),
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "details",
            "storageKey": null,
            "args": null,
            "concreteType": "nodeDetailsType",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              (v4/*: any*/),
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "logo",
                "storageKey": null,
                "args": null,
                "concreteType": "imageType",
                "plural": false,
                "selections": (v8/*: any*/)
              },
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "image",
                "storageKey": null,
                "args": null,
                "concreteType": "imageType",
                "plural": false,
                "selections": (v8/*: any*/)
              }
            ]
          },
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "floor",
            "storageKey": null,
            "args": null,
            "concreteType": "floorType",
            "plural": false,
            "selections": [
              (v2/*: any*/),
              (v7/*: any*/)
            ]
          },
          (v7/*: any*/)
        ]
      }
    ]
  },
  "params": {
    "operationKind": "query",
    "name": "SearchRendererQuery",
    "id": null,
    "text": "query SearchRendererQuery(\n  $query: String\n) {\n  search(query: $query) {\n    name\n    details {\n      description\n      category\n      logo {\n        data\n        id\n      }\n      image {\n        data\n        id\n      }\n    }\n    floor {\n      name\n      id\n    }\n    id\n  }\n}\n",
    "metadata": {}
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '14e1d8238b5f6c11cba0b60d16089e63';
module.exports = node;
