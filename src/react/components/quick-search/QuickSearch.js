import React, { Component } from 'react';
import './QuickSearch.scss';

class QuickSearch extends Component {
    constructor(props){
        super(props);
        
        this.search = this.search.bind(this);
    }
    search(searchString) {
        console.log(searchString);
        this.props.emitter.emit('onClick', {
            searchString: searchString
        });
    }
    render() {
        return (
            <div className='quick-search'>
                <h2>{this.props.title}</h2>
                <ul>
                    <li onClick={() => { this.search('shoes') }}>
                        <img src={require('../../assets/images/icons/baseline_wc_black_48dp.png')}/>
                        <h3>Toilet</h3>
                    </li>
                    <li onClick={() => { this.search('shoes') }}>
                        <img src={require('../../assets/images/icons/baseline_wc_black_48dp.png')}/>
                        <h3>Toilet</h3>
                    </li>
                    <li onClick={() => { this.search('shoes') }}>
                        <img src={require('../../assets/images/icons/baseline_wc_black_48dp.png')}/>
                        <h3>Toilet</h3>
                    </li>
                    <li onClick={() => { this.search('shoes') }}>
                        <img src={require('../../assets/images/icons/baseline_wc_black_48dp.png')}/>
                        <h3>Toilet</h3>
                    </li>
                    <li onClick={() => { this.search('shoes') }}>
                        <img src={require('../../assets/images/icons/baseline_wc_black_48dp.png')}/>
                        <h3>Toilet</h3>
                    </li>
                </ul>
            </div>
        )
    }
}

export default QuickSearch;