import React, { Component } from 'react';
import { QueryRenderer } from 'react-relay';
import { graphql } from 'babel-plugin-relay/macro';
import environment from '../../../graphql/environment';
import { Draggable } from 'gsap/all';
import ThrowPropsPlugin from '../../plugins/ThrowPropsPlugin.js';
import './Directory.scss';

const plugin = ThrowPropsPlugin;

class Directory extends Component {
    constructor(props) {
        super(props);

        this.state = {
            currentFloor: 'Ground Floor'
        }
    }
    initDraggable() {
        setTimeout( () => {
            const directoryContainer = document.querySelector('.directory');
            const draggable = document.querySelector('.directory__draggable');
                Draggable.create( draggable, {
                    type: 'y',
                    bounds: directoryContainer,
                    edgeResistance: 0.75,
                    throwProps: true,
                    lockAxis: true,
                    dragClickables: true
                })
        }, 0);
    }
    render() {
        return (
            <QueryRenderer
                environment={environment}
                query={graphql`
                    query DirectoryQuery($floor: String!) {
                        stores(floor: $floor) {
                            name,
                            type,
                            details {
                                category,
                                description
                            }
                        }
                    }
                `}
                variables={{
                    floor: this.state.currentFloor
                }}
                render={
                    ({ error, props }) => {
                        // TODO error
                        // if (error) return <div>Error!</div>;
                        // TODO preloader
                        if (!props) return <div>Loading!</div>;

                        // TODO animation
                        return <div className='directory'>
                            <div className='directory__draggable'>
                                a b c d e f
                                {
                                    this.initDraggable()
                                }
                            </div>
                        </div>
                    }
                }
            />
        )
    }
}

export default Directory;
