/**
 * @flow
 * @relayHash 45ef4a7dd9903a633e030bf1a4426524
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type FloorTabsQueryVariables = {||};
export type FloorTabsQueryResponse = {|
  +floors: ?$ReadOnlyArray<?{|
    +id: string,
    +name: ?string,
  |}>
|};
export type FloorTabsQuery = {|
  variables: FloorTabsQueryVariables,
  response: FloorTabsQueryResponse,
|};
*/


/*
query FloorTabsQuery {
  floors {
    id
    name
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "floors",
    "storageKey": null,
    "args": null,
    "concreteType": "floorType",
    "plural": true,
    "selections": [
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "id",
        "args": null,
        "storageKey": null
      },
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "name",
        "args": null,
        "storageKey": null
      }
    ]
  }
];
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "FloorTabsQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": [],
    "selections": (v0/*: any*/)
  },
  "operation": {
    "kind": "Operation",
    "name": "FloorTabsQuery",
    "argumentDefinitions": [],
    "selections": (v0/*: any*/)
  },
  "params": {
    "operationKind": "query",
    "name": "FloorTabsQuery",
    "id": null,
    "text": "query FloorTabsQuery {\n  floors {\n    id\n    name\n  }\n}\n",
    "metadata": {}
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'c1e55dddac36808cef59f80a877bb884';
module.exports = node;
