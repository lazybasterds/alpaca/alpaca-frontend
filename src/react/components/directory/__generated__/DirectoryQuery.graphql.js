/**
 * @flow
 * @relayHash 6ee1d12bea81dc6b5eb11ae311bac6f8
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type DirectoryQueryVariables = {|
  floor: string
|};
export type DirectoryQueryResponse = {|
  +stores: ?$ReadOnlyArray<?{|
    +name: ?string,
    +type: ?string,
    +details: ?{|
      +category: ?string,
      +description: ?string,
    |},
  |}>
|};
export type DirectoryQuery = {|
  variables: DirectoryQueryVariables,
  response: DirectoryQueryResponse,
|};
*/


/*
query DirectoryQuery(
  $floor: String!
) {
  stores(floor: $floor) {
    name
    type
    details {
      category
      description
    }
    id
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "floor",
    "type": "String!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "floor",
    "variableName": "floor",
    "type": "String!"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "name",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "type",
  "args": null,
  "storageKey": null
},
v4 = {
  "kind": "LinkedField",
  "alias": null,
  "name": "details",
  "storageKey": null,
  "args": null,
  "concreteType": "nodeDetailsType",
  "plural": false,
  "selections": [
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "category",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "description",
      "args": null,
      "storageKey": null
    }
  ]
};
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "DirectoryQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": (v0/*: any*/),
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "stores",
        "storageKey": null,
        "args": (v1/*: any*/),
        "concreteType": "nodeType",
        "plural": true,
        "selections": [
          (v2/*: any*/),
          (v3/*: any*/),
          (v4/*: any*/)
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "DirectoryQuery",
    "argumentDefinitions": (v0/*: any*/),
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "stores",
        "storageKey": null,
        "args": (v1/*: any*/),
        "concreteType": "nodeType",
        "plural": true,
        "selections": [
          (v2/*: any*/),
          (v3/*: any*/),
          (v4/*: any*/),
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "id",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  },
  "params": {
    "operationKind": "query",
    "name": "DirectoryQuery",
    "id": null,
    "text": "query DirectoryQuery(\n  $floor: String!\n) {\n  stores(floor: $floor) {\n    name\n    type\n    details {\n      category\n      description\n    }\n    id\n  }\n}\n",
    "metadata": {}
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'fb67c45422738b0959daa6885a6dfcfb';
module.exports = node;
