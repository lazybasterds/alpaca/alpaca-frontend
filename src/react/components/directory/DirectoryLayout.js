import React, { Component } from 'react';
import './DirectoryLayout.scss';
import Directory from './Directory';
import FloorTabs from './FloorTabs';

class DirectoryLayout extends Component {
    render() {
        if (!this.props.visible) return null;
        else return (
            <div className={`directory-layout ${this.props.classes.join(' ')}`}>
                <FloorTabs />
                <Directory />
            </div>
        )
    }
}

export default DirectoryLayout;