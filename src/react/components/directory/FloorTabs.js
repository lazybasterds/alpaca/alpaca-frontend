import React, { Component } from 'react';
import { QueryRenderer } from 'react-relay';
import { graphql } from 'babel-plugin-relay/macro';
import environment from '../../../graphql/environment';
import { Draggable } from 'gsap/all';
import ThrowPropsPlugin from '../../plugins/ThrowPropsPlugin.js';
import './FloorTabs.scss';
import Hammer from 'react-hammerjs';

const plugin = ThrowPropsPlugin;

class FloorTabs extends Component {
    constructor(props) {
        super(props);

        this.floorTabClicked = this.floorTabClicked.bind(this);
        this.state = {
            floors: [],
            currentFloor: ''
        }
    }
    initDraggable() {
        setTimeout( () => {
            const floorTabContainer = document.querySelector('.floor-tabs');
            const draggable = document.querySelector('.floor-tabs__draggable');
                Draggable.create( draggable, {
                    type: 'y',
                    bounds: floorTabContainer,
                    edgeResistance: 0.75,
                    throwProps: true,
                    lockAxis: true,
                    dragClickables: true,
                    onClick: (e) => {
                        console.log(e);
                        console.log(this.props.floors);
                    }
                });
        }, 0);
    }
    componentWillReceiveProps(floors) {
        this.setState({
            floors: floors
        });
    }
    floorTabClicked(floorId) {
        console.log(floorId);
    }
    render() {
        return (
            <QueryRenderer
                environment={environment}
                query={graphql`
                    query FloorTabsQuery {
                        floors {
                            id,
                            name
                        }
                    }
                `}
                render={
                    ({ error, props }) => {
                        // TODO error
                        // if (error) return <div>Error!</div>;
                        // TODO preloader
                        if (!props) return <div>Loading!</div>;

                        // TODO animation
                        return <div className='floor-tabs'>
                            <div className='floor-tabs__draggable'>
                                {
                                    this.initDraggable()
                                }
                                {
                                    props.floors.map( (floor, i) => {
                                        return <a key={i} data-key={floor.id}>
                                            {floor.name}
                                        </a>;
                                    })
                                }
                                
                            </div>
                        </div>
                    }
                }
            />
        )
    }
}

export default FloorTabs;
